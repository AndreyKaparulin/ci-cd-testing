'use strict';

const express = require('express');

// Constants
const PORT = 3000;


// App
const app = express();


app.get('/', (req, res) => {
  res.send('type her new message 111');
});

app.get('/assertUrl', (req, res) => {
  res.send('Hello world');
});
app.listen(PORT);
console.log(`server running on ${PORT}`);